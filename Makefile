# SPDX-License-Identifier: ISC

UPSTREAM_BRANCH=	trunk
RELEASE_SOURCE=		build

.include <sz.downstream.mk>
